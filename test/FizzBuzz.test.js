// import default from modules
import { expect } from 'chai';
import { fizzBuzz, isNumberIsMultipleOf3And5, isNumberOnlyMultipleOf3, isNumberNotMultpleOf3Or5, isNumberOnlyMultipleOf5 } from '../src/FizzBuzz';

// Write ES6 mocha tests with Chai assertions
describe('isNumberIsOnlyMultipleOf3', () => {
  it('should return true if a number is multiple of 3 and not multiple of 5', () => {
    expect(isNumberOnlyMultipleOf3(3)).to.equal(true);
    expect(isNumberOnlyMultipleOf3(9)).to.equal(true);
    expect(isNumberOnlyMultipleOf3(1)).to.equal(false);
    expect(isNumberOnlyMultipleOf3(15)).to.equal(false);
    });
});

describe('isNumberOnlyMultipleOf5', () => {
  it('should return true if a number is multiple of 5 and not multiple of 3', () => {
    expect(isNumberOnlyMultipleOf5(3)).to.equal(false);
    expect(isNumberOnlyMultipleOf5(5)).to.equal(true);
    expect(isNumberOnlyMultipleOf5(15)).to.equal(false);
    expect(isNumberOnlyMultipleOf5(1)).to.equal(false);
    expect(isNumberOnlyMultipleOf5(4)).to.equal(false);
    });
});

describe('isNumberNotMultpleOf3Or5', () => {
  it('should return true if a number is not multiple of 5 or multiple of 3', () => {
    expect(isNumberNotMultpleOf3Or5(3)).to.equal(false);
    expect(isNumberNotMultpleOf3Or5(5)).to.equal(false);
    expect(isNumberNotMultpleOf3Or5(15)).to.equal(false);
    expect(isNumberNotMultpleOf3Or5(1)).to.equal(true);
    expect(isNumberNotMultpleOf3Or5(4)).to.equal(true);
    });
});

describe('isNumberIsMultipleOf3And5', () => {
  it('should return true if a number is multiple of 5 and multiple of 3', () => {
    expect(isNumberIsMultipleOf3And5(3)).to.equal(false);
    expect(isNumberIsMultipleOf3And5(5)).to.equal(false);
    expect(isNumberIsMultipleOf3And5(1)).to.equal(false);
    expect(isNumberIsMultipleOf3And5(15)).to.equal(true);
    expect(isNumberIsMultipleOf3And5(4)).to.equal(false);
    });
});

describe('FizzBuzz', () => {
  it('should return one line for each number from 1 to 100', () => {
    expect(fizzBuzz().split(/\r\n|\r|\n/).length).to.equal(100);
  });

  it('should print the numbers from 1 to 100 each in a different line if the number is not multiple of 3 or 5', () => {
    const separatedNumbers = fizzBuzz().split(/\r\n|\r|\n/).map(number => +number).filter(num => num === NaN).filter(num => num % 3 !== 0 || num % 5 !== 0);
    const numberFromOneToAHundred = Array.from({length: 100}, (_, i) => i + 1).filter(num => num % 3 !== 0 || num % 5 !== 0);

    separatedNumbers.map((number, index) => {
      expect(separatedNumbers[index]).to.equal(numberFromOneToAHundred[index]);
    })
  });


  it('should print Fizz for the numbers that are multiples of 3', () => {
    const separatedString = fizzBuzz().split(/\r\n|\r|\n/);
    const multiplesOf3 = Array.from({length: 100}, (_, i) => i + 1).filter(num => num % 3 === 0 && num % 5 !== 0);

    multiplesOf3.map((value) => {
      expect(separatedString[value - 1]).to.equal('Fizz');
    })
  });

  it('should print Buzz for the numbers that are multiples of 5', () => {
    const separatedString = fizzBuzz().split(/\r\n|\r|\n/);
    const multiplesOf5 = Array.from({length: 100}, (_, i) => i + 1).filter(num => num % 5 === 0 && num % 3 !== 0);

    multiplesOf5.map((value) => {
      expect(separatedString[value - 1]).to.equal('Buzz');
    })
  });

  it('should print FizzBuzz for the numbers that are multiples of 3 and 5', () => {
    const separatedString = fizzBuzz().split(/\r\n|\r|\n/);
    const multiplesOf5 = Array.from({length: 100}, (_, i) => i + 1).filter(num => num % 3 === 0 && num % 5 === 0);

    multiplesOf5.map((value) => {
      expect(separatedString[value - 1]).to.equal('FizzBuzz');
    })
  });
});
