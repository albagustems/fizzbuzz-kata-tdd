# Kata's specifications
- Write a program that prints one line for each number from 1 to 100.
- Usually just print the number itself.
- For multiples of three print `Fizz` instead of the number.
- For the multiples of five print `Buzz` instead of the number.
- For numbers which are multiples of both three and five print `FizzBuzz` instead of the number.

## Installation

Go to the root directory (where the `package.json` is located) and run:

```bash
npm install
```

## Running tests

Run the tests once:

```bash
npm test
```

Run the test and re-run them once a file changes:

```bash
npm run test:watch
```