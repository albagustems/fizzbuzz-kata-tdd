// Enjoy arrow functions

export const isNumberOnlyMultipleOf3 = (num) => {
    return num % 3 === 0 && num % 5 !== 0;
}

export const isNumberOnlyMultipleOf5 = (num) => {
    return num % 3 !== 0 && num % 5 === 0;
}

export const isNumberIsMultipleOf3And5 = (num) => {
    return num % 3 === 0 && num % 5 === 0;
}

export const isNumberNotMultpleOf3Or5 = (num) => {
    return num % 3 !== 0 && num % 5 !== 0;
}

export const fizzBuzz = () => {
    const lines = []
    for (let currentNumber = 1; currentNumber <= 100; currentNumber++) {
        isNumberOnlyMultipleOf3(currentNumber) && lines.push('Fizz');
        isNumberOnlyMultipleOf5(currentNumber) && lines.push('Buzz');
        isNumberIsMultipleOf3And5(currentNumber) && lines.push('FizzBuzz');
        isNumberNotMultpleOf3Or5(currentNumber) && lines.push(currentNumber.toString());
    }

    return lines.join('\n')
}
